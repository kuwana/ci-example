package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"

	"github.com/labstack/echo"
	_ "github.com/lib/pq"
)

type Employee struct {
	Code string
	Name string
	Age  string
}

func main() {
	e := echo.New()
	fmt.Println(os.Getenv("DATABASE_URL"))
	e.GET("/", func(c echo.Context) error {
		fmt.Println(os.Getenv("DATABASE_URL"))
		db, err := sql.Open("postgres", os.Getenv("DATABASE_URL")+os.Getenv("SSL_MODE"))
		defer db.Close()

		if err != nil {
			fmt.Println(err)
		}
		rows, err := db.Query("select * from employee")
		if err != nil {
			fmt.Println(err)
		}
		// var es []Employee
		var e Employee
		for rows.Next() {
			rows.Scan(&e.Code, &e.Name, &e.Age)
			fmt.Println(e.Name)
			break
		}
		return c.String(http.StatusOK, "selected name from db: "+e.Name)
	})
	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}
